<?php 
    include '../../controllers/request.php';
    $requestHandler = new RequestHandler();
    $reqRes = $requestHandler->flagRequest($_SERVER, json_decode(file_get_contents('php://input'), true));
    if($reqRes["success"]){
        include '../../controllers/sms.php';
        $sms = new SMS();
        $reqRes = $sms->sendQueuedSMS($reqRes["payLoad"]);
    }
    echo json_encode($reqRes);
?>