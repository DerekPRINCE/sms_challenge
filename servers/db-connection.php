<?php
    ini_set('display_errors', 1);
    ini_set('log_errors',true);

    @require_once('adodb/adodb.inc.php');
    @require_once('adodb/adodb-active-record.inc.php');

    $db_type  = 'mysqli';
    $db_host  = '127.0.0.1:3306';//127.0.0.1:5432
    $db_user  = 'root';
    $db_pass  = '';
    $db_name  = 'wazinsure_sms_challenge';//

    $db = ADONewConnection($db_type);

    if(! @$db->Connect($db_host,$db_user,$db_pass,$db_name)){
        echo json_encode(array(
            "success"=>false,
            "errors"=>array("Could not connect to database."),
            "status_code"=>0,
            "status_message"=>'Failed.',
            "message"=>"Database is gone.",
            "data"=>null
        ));
        exit();
    }

    $db->SetFetchMode(ADODB_FETCH_ASSOC);

    ADODB_Active_Record::SetDatabaseAdapter( $db );
    $pubKey = "";
?>
