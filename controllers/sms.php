<?php 
    require_once '../../vendor/autoload.php';
    class SMS extends DatabaseHandler {

        private $debug, $configModel, $errors;
        public function __construct($debug = NULL){
            $this->debug = $debug;
            parent::__construct($this->debug);
            $this->errors = array();
            $this->configModel = json_decode(file_get_contents("../../models/sms.json"), true);
        }

        public function queuedSMS ($payLoad) {
            $validInput = $this->utils->validateModel($payLoad, $this->configModel["valid_queue"]);
            if($validInput["success"]){
                $payLoad["code"] = $this->utils->generateRandom(11111, 99999, 5);
                $dbRes = $this->insert($this->dbTables[0], $payLoad);
                if($dbRes[0] == 1){
                    return array("success"=>true,"errors"=>null,"status_code"=>1,"status_message"=>'Success.',"message"=>'SMS queued',"data"=>$payLoad);
                }
                array_push($this->errors, $dbRes[1]);
                return array("success"=>true,"errors"=>$this->errors,"status_code"=>0,"status_message"=>'Failed.',"message"=>'SMS not queued',"data"=>null);
            }
            return $validInput;
        }

        public function sendQueuedSMS ($payLoad){
            $validInput = $this->utils->validateModel($payLoad, $this->configModel["valid_send_queued"]);
            if($validInput["success"]){
                $gateWay = $payLoad["gateWay"];
                $smsModels = $this->fetchRows($this->dbTables[0], array("status_code"=>0))[2];
                if(!empty($smsModels)){
                    $sentMessages = 0;
                    $responses = array();
                    foreach ($smsModels as $smsModel) {
                        $response = $this->sendSMS($gateWay, $smsModel);
                        array_push($responses, $response);
                        $sentMessages ++;
                    }
                    return array("status"=>true, "status_code"=>1, "message"=>'Success', "errors"=>null, "data"=>array("sent"=>$sentMessages, "responses"=>$responses));
                }
                return array("status"=>true, "status_code"=>0, "message"=>'Failed', "errors"=>array('No queued SMS(s).'), "data"=>null);
            }
            return $validInput;
        }

        public function sendSMS ($gateWay, $smsModel) {
            $validInput = $this->utils->validateModel($smsModel, $this->configModel["valid_send"]);
            if($validInput["success"]){
                switch($gateWay){
                    case 1: 
                        return $this->aftlkingSMS($smsModel);
                    case 2: 
                        return $this->nexmoSMS($smsModel);
                    case 3:
                        return $this->infobipSMS($smsModel);
                    default:
                        return array("status"=>true, "status_code"=>0, "message"=>'Failed', "errors"=>array('Unknown SMS gateway.'), "data"=>null);
                }
            }
            return $validInput;
        }

        private function aftlkingSMS ($smsModel) {
            $AT = new \AfricasTalking\SDK\AfricasTalking('', '');
            $sms = $AT->sms();
            try {
                // Thats it, hit send and we'll take care of the rest
                return $sms->send([
                    'to' => $smsModel['to_id'],
                    'from' => $smsModel['from_id'],
                    'text' => $smsModel['msg_text']
                ]);
            } catch (Exception $e) {
                echo "Error: ".$e->getMessage();
            }
        }

        private function nexmoSMS ($smsModel) {
            $basic  = new \Nexmo\Client\Credentials\Basic('55c12ca7', 'W01n0DICaZ5OQfPl');
            $client = new \Nexmo\Client($basic);
            //send message using simple api params
            return $client->message()->send([
                'to' => $smsModel['to_id'],
                'from' => $smsModel['from_id'],
                'text' => $smsModel['msg_text']
            ]);
        }

        private function infobipSMS ($smsModel) {
            $headers = array(
                "accept: application/json",
                "Authorization: Basic " . base64_encode(":"),
                "content-type: application/json"
            );
            $messages = array();
            array_push($messages, array(
                "from"=>$smsModel['from_id'],
                "destinations"=>array(
                    array(
                        "to"=>$smsModel['to_id'],
                        "messageId"=>$this->dates->timeStamp()
                    ),
                    array(
                        "to"=>$smsModel['to_id']
                    )
                ),
                "text"=>$smsModel["msg_text"],
                "flash"=>false,
                "language"=>array(
                    "languageCode"=>"EN"
                ),
                "transliteration"=>"ENGLISH",
                "intermediateReport"=>true,
                "notifyUrl"=>"/callback.php",
                "notifyContentType"=>"application/json",
                "callbackData"=>"DLR callback data",
                "validityPeriod"=>720
            ));
            
            $payLoad = array (
                "bulkId"=>$this->dates->timeStamp(),
                "messages"=>$messages
            );            
            return json_decode($this->utils->cURLRequest('POST', $payLoad, 'https://1pp9k.api.infobip.com/sms/2/text/advanced', $headers), true);
        }
    }
