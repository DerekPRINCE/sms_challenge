/*
Navicat MySQL Data Transfer

Source Server         : localhost:3309
Source Server Version : 50524
Source Host           : localhost:3309
Source Database       : wazinsure_sms_challenge

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2019-12-29 11:56:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` longtext,
  `to_id` varchar(12) DEFAULT NULL,
  `from_id` varchar(100) DEFAULT NULL,
  `msg_text` varchar(100) DEFAULT '',
  `queued_at` datetime DEFAULT NULL,
  `status_code` int(11) DEFAULT '0',
  `status_name` varchar(100) DEFAULT 'not sent',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', '81003', '+25470682855', 'Zaidi\r\nTechnologies', 'Hi Derek, your account balance is KES3000.00', '2019-03-13 23:50:34', '0', 'not sent');
